---
firewall_internal_networks:
  - 10.23.0.0/24
  - 2001:470:ec7b:1::/64

blackmagic_default_mode: 720p50
blackmagic:
  urls:
    desktopvideo: http://storage/blackmagic/Blackmagic_Desktop_Video_Linux_12.5/deb/x86_64/desktopvideo_12.5a15_amd64.deb
    desktopvideo_gui: http://storage/blackmagic/Blackmagic_Desktop_Video_Linux_12.5/deb/x86_64/desktopvideo-gui_12.5a15_amd64.deb
  dkms_version: 12.5a15

domain: video.dc23.lan
prometheus_inventory_hostname: storage
dhcp_range: 10.23.0.128,10.23.0.254,6h
staticips:
  hosts:
  # The online hosts should probably be renumbered to be more in line with the
  # onsite hosts. Also, voctomixX.online might need to be renamed to
  # vogolX.online to avoind clashing with hostnames for onsite
  - hostname: router
    ip: 10.23.0.1
  - hostname: storage
    aliases:
      - prometheus
      - grafana
    ip: 10.23.0.2
    mac: d8:bb:c1:4e:e8:f5
  - hostname: voctomix1
    ip: 10.23.0.10
    disk: /dev/nvme0n1
    mac: 74:56:3c:62:fe:7b
  - hostname: voctomix2
    ip: 10.23.0.20
    disk: /dev/nvme0n1
    mac: d8:bb:c1:4e:eb:e2
    noshim: true
  - hostname: voctomix3
    ip: 10.23.0.30
    disk: /dev/nvme0n1
    mac: 74:56:3c:3f:c3:93
  - hostname: opsis1
    ip: 10.23.0.11
    # set this for turbot that installs on internal storage
    disk: /dev/mmcblk1
    mac: 00:08:a2:0c:f3:12
  - hostname: opsis2
    ip: 10.23.0.21
    mac: 18:c0:4d:5a:88:a3
    disk: /dev/nvme0n1
  - hostname: opsis3
    ip: 10.23.0.31
    mac: d8:bb:c1:4e:e7:51
    noshim: true
  - hostname: encoder1
    ip: 10.23.0.101
    mac: dc:4a:3e:97:6a:dd
  - hostname: encoder2
    ip: 10.23.0.102
    mac: dc:4a:3e:96:f2:57
  - hostname: encoder3
    ip: 10.23.0.104
    mac: dc:4a:3e:9a:da:b6
    aliases:
      - test-station
  - hostname: obs-local
    ip: 10.23.0.4
    mac: 18:c0:4d:5e:61:3f
  - hostname: laptop1
    ip: 10.23.0.111
    mac: e4:a8:df:f5:b3:ee
    disk: /dev/nvme0n1
  - hostname: laptop2
    ip: 10.23.0.112
    mac: b0:0c:d1:31:8e:49
  - hostname: laptop3
    ip: 10.23.0.113
    mac: 08:97:98:dd:78:fa
    disk: /dev/nvme0n1
  - hostname: backend
    ip: 10.23.32.2
    mac: 52:54:00:3e:7d:19
    aliases:
      - backend.live.debconf.org
  - hostname: onsite
    ip: 10.23.32.3
    mac: 52:54:00:3e:7d:19
    aliases:
      - onsite.live.debconf.org

  write_hosts: true
  # interfaces are only written for hosts with staticips role
  write_interfaces: true
