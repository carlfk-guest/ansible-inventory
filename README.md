# DebConf Video Team Ansible Inventory

This Ansible inventory repo is where we keep our configuration for running the
[ansible roles](https://salsa.debian.org/debconf-video-team/ansible) in
production for various DebConfs.

If you are wanting to use our setup, fork and modify this repo to use with the
ansible roles we set up.

The current ansible vault password file can be found at:

    vittoria.debian.org:/home/stefanor/master.vault

To encrypt a variable using ansible vault, use the following command:

    ansible-vault encrypt_string --vault-password-file debconfxx.vault 'hunter2' --name 'my_super_variable'

To decrypt a variable that uses ansible vault, use the following command:

    ansible localhost -m debug --vault-password-file debconfxx.vault -a var="my_super_variable" -e "@file.yml"

To change the key used to encrypt vault secrets:

    ./rekey.py --old-vault-password-file debconfxx.vault --new-vault-password-file debconfxy.vault
